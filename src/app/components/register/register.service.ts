import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  URL="http://localhost:3000/"

  addUser(body: Usuario ){
    return this.http.post(`${this.URL}innov/register`, body);
  }

}
